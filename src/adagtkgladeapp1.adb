with Gtk.Main;
with Gtk.Builder; use Gtk.Builder;
with Gtk.Window;      use Gtk.Window;
with Gtk.Grid;        use Gtk.Grid;
with Gtk.Widget;     use Gtk.Widget;
with Gtk.Button;      use Gtk.Button;
with Gtkada.Handlers; use Gtkada.Handlers;
with GLib; use GLib;
with Glib.Error; use Glib.Error;
with Ada.Text_IO; use Ada.Text_IO;
with Glib.Object;

--with Main_quit;
with adagtkgladeapp1_callbacks; use adagtkgladeapp1_callbacks;

procedure adagtkgladeapp1 is
   FrmMain : Gtk_Window;
   Grid   : Gtk_Grid;
   ButtonOne : Gtk_Button;
   ButtonTwo : Gtk_Button;
   BtnQuit : Gtk_Button;
   builder : Gtk_Builder;
   returnCode : GUint;-- GLib
   fileError : aliased GError;

begin

   --  Initialize GtkAda.
   Gtk.Main.Init;

-- from gtkada-builder.ads:
--     Step 1: create a Builder and add the XML data, just as you would a
--             standard Gtk.Builder:

  -- construct a Gtk_Builder instance and load our UI description
   Gtk_New (builder);
   returnCode := Add_From_File (builder, "adagtkgladeapp1.ui", fileError'Access);--Add_From_File (Builder, "adagtkgladeapp1.ui", fileError'Access);

   if fileError /= null then
      Put_Line ("Error : " & Get_Message (fileError));
      Error_Free (fileError);
      return;
   end if;

--     Step 2: add calls to "Register_Handler" to associate your handlers
--             with your callbacks.
--
  -- connect signal handlers to the constructed widgets
   FrmMain := Gtk_Window (Builder.Get_Object ("frmmain"));
 

--     Step 3: call Do_Connect.
--Note: not done, because we are connecting explicitly


   -- When the window emits the "delete-event" signal (which is emitted
   --  by GTK+ in response to an event coming from the window manager,
   --  usually as a result of clicking the "close" window control), we
   --  ask it to call the on_delete_event() function as defined above.
   -- This can also be triggered by a button (in this case) or menu click.
   FrmMain.On_Delete_Event (FrmMain_OnDeleteEvent'Access);

   -- connect the "destroy" signal
   -- Note: does not call Gtk.Main.Main_Quit directly
   FrmMain.On_Destroy (FrmMain_OnDestroy'Access);

   -- Here we construct the container that is going pack our buttons
   Grid := Gtk_Grid (Builder.Get_Object ("grid"));
   --  in case we need access to grid

   -- create a button with label
   ButtonOne := Gtk_Button (Builder.Get_Object ("btn1"));
   -- connect the click signal
   ButtonOne.On_Clicked (BtnOneOrTwo_OnClicked'Access);

   -- create another button with label
   ButtonTwo := Gtk_Button (Builder.Get_Object ("btn2"));
   -- connect the click signal
   ButtonTwo.On_Clicked (BtnOneOrTwo_OnClicked'Access);

   -- create a button with label
   BtnQuit := Gtk_Button (Builder.Get_Object ("btnquit"));
   -- Connect the click signal
   -- Note:this does not do nothing, while the Widget_Callback.Object_Connect call below
   --  connects an alternate click event receiving the main form object
   BtnQuit.On_Clicked (BtnQuit_OnClicked'Access);
   -- Connect the "clicked" signal of the button to a callback function,
   --  that will close the Win when button is clicked.
   -- This is the alternate version of the callback, 
   --  that passes the main form object instead of the button object.
   Widget_Callback.Object_Connect (
      BtnQuit,
      "clicked",
      Widget_Callback.To_Marshaller (BtnQuit_OnClicked2'Access),
      FrmMain
   );
   
   -- Now that we are done packing our widgets, we show them all
   -- in one go, by calling FrmMain.Show_All.
   -- This call recursively calls Show on all widgets
   -- that are contained in the window, directly or indirectly.
   --  FrmMain.Show_All;
   -- Get the Gtk_Widget object from the Gtkada.Builder API
   -- for our commandWindow so it will be shown.
   Gtk.Widget.Show_All (Gtk_Widget (FrmMain));--Builder.Get_Object (Builder, "frmmain")));


   -- All GTK applications must have a Gtk.Main.Main. This is the main message loop.
   -- Control ends here and waits for an event to occur 
   -- (like a key press or a mouse event),  until Gtk.Main.Main_Quit is called.
   Gtk.Main.Main;

--     Step 4: when the application terminates or all Windows described through
--             your builder should be closed, call Unref to free memory
--             associated with the Builder.
   Unref (builder);
   
end adagtkgladeapp1;
