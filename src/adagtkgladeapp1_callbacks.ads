with Gtk.Window;      use Gtk.Window;
with Gtk.Widget;  use Gtk.Widget;
with Gtk.Button;  use Gtk.Button;
with Glib.Object;

with Gdk.Event;

package adagtkgladeapp1_callbacks is
   function FrmMain_OnDeleteEvent (Self: access Gtk_Widget_Record'Class; Event: Gdk.Event.Gdk_Event) return Boolean;
   procedure FrmMain_OnDestroy (Self: access Gtk_Widget_Record'Class);
   procedure BtnOneOrTwo_OnClicked (Self: access Gtk_Button_Record'Class);
   procedure BtnQuit_OnClicked (Self: access Gtk_Button_Record'Class);
   procedure BtnQuit_OnClicked2 (Self: access Gtk_Widget_Record'Class);
end adagtkgladeapp1_callbacks;